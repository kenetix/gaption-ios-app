//
//  AppDelegate.h
//  Gaption
//
//  Created by Kenneth Ho on 1/21/15.
//  Copyright (c) 2015 Kenneth Ho. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

