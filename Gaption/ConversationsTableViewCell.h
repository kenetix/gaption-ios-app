//
//  ConversationsTableViewCell.h
//  Gaption
//
//  Created by Kenneth Ho on 2/6/15.
//  Copyright (c) 2015 Kenneth Ho. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConversationsTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *screenNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *recentMessage;
@property (weak, nonatomic) IBOutlet UIImageView *profileAvatar;

- (void)updateCell:(NSDictionary *)data;


@end