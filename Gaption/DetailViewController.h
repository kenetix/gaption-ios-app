//
//  DetailViewController.h
//  Gaption
//
//  Created by Kenneth Ho on 1/21/15.
//  Copyright (c) 2015 Kenneth Ho. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end

