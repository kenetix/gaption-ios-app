//
//  main.m
//  Gaption
//
//  Created by Kenneth Ho on 1/21/15.
//  Copyright (c) 2015 Kenneth Ho. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
