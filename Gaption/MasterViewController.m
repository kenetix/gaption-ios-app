//
//  MasterViewController.m
//  Gaption
//
//  Created by Kenneth Ho on 1/21/15.
//  Copyright (c) 2015 Kenneth Ho. All rights reserved.
//
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "ConversationsTableViewCell.h"
#import "MasterViewController.h"
#import "DetailViewController.h"

@interface MasterViewController ()

@property NSMutableArray *objects;
@end

@implementation MasterViewController

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)loadJSON:(NSString *)url {

    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id jsonData) {
              
        if([jsonData[@"status_code"] intValue] == 200 && jsonData[@"items"] && [jsonData[@"items"] count] > 0){
            for(NSDictionary *item in jsonData[@"items"]){ // loop and add key to objects variable
                [self.objects addObject:item];
            }
            
            dispatch_async(dispatch_get_main_queue(),^{
                [self.tableView reloadData];
                [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES]; // remove progress view
                NSLog(@"lala %lu", (unsigned long)[jsonData[@"items"] count]);
            });
            
        }
        else if([jsonData[@"status_code"] intValue] == 403){ // not authorized
            
            [self performSegueWithIdentifier: @"showDetail" sender: self];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.objects = [[NSMutableArray alloc] init];
    [self loadJSON:@"https://gaption.com/index.php?system=feed&fields=profile,links,status,responses,comments,gallery&limit=5&type=1&token=0117720b15c22d74460f62fc1c0330f4"];
    //[self.tableView registerNib:[UINib nibWithNibName:@"FeedBox" bundle:nil] forCellReuseIdentifier:@"Cell"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Segues
/*
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSDate *object = self.objects[indexPath.row];
        [[segue destinationViewController] setDetailItem:object];
    }
}*/

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ConversationsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    if([self.objects count] > 0){ // ensures the object array isn't empty
        NSDictionary *object = self.objects[indexPath.row];
        [cell updateCell:object]; // calls the method to update the constraints and feed variables
    }
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}




@end
