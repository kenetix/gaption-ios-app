//
//  ConversationsTableViewCell.m
//  Gaption
//
//  Created by Kenneth Ho on 2/6/15.
//  Copyright (c) 2015 Kenneth Ho. All rights reserved.
//

#import "ConversationsTableViewCell.h"

@implementation ConversationsTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}




- (void)updateCell:(NSDictionary *)data{
    
    self.profileAvatar.layer.cornerRadius = 30; // half the image width/height
    self.profileAvatar.clipsToBounds = YES;

    self.screenNameLabel.text = data[@"profile"][@"info"][@"displayname"];
    
    // self.Somebuttonlabel.titleLabel.text = @"haha";
    
    if ([self.screenNameLabel.text  isEqual: @"Wallace Ho"]) {
        self.screenNameLabel.text = data[@"profile"][@"info"][@"username"];

        //   self.retweetView.hidden = NO;
        //  [self.imageViewTopSpace setConstant:30];
      //  [self.viewTopSpace setConstant:300];
        
    }
}

@end